const path = require('path');


module.exports = {
    dbUrl: 'mongodb://localhost/Chat',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
    }
};
