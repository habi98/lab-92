const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const nanoid = require('nanoid');
const config = require('./config');

const users = require('./app/users');
const User = require('./models/User');
const Messages = require('./models/Messages');

const app = express();
const expressWs = require('express-ws')(app);

app.use(express.json());
app.use(cors());

const port = 8000;

const activeConnections = {};


app.ws('/chat', async (ws, req) => {
    if (!req.query.token) {
        return ws.close()
    }

    const user = await User.findOne({token: req.query.token});

    if (!user) {
        return ws.close()
    }

    const id = nanoid();

    console.log('client connected=' + id);

    activeConnections[id] = {ws, user};


        const username = Object.keys(activeConnections).map(connId => {
            const connection = activeConnections[connId];
            return connection.user.username
        });

        ws.send(JSON.stringify({
            type: 'ACTIVE_USERS',
            username
        }));



    ws.send(JSON.stringify({
            type: 'LATEST_MESSAGES',
                messages:  await Messages.find().sort({'_id': -1}).limit(30)
        }));

   Object.keys(activeConnections).forEach(connId => {
        const conn = activeConnections[connId].ws;
        conn.send(JSON.stringify({
            type: 'ACTIVE_USERS',
              username
        }));
    });



    ws.on('message', msg => {
        const decodedMessage = JSON.parse(msg);
        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':

                const message = {
                    name: decodedMessage.username,
                    text: decodedMessage.text
                };

                const messages = new Messages(message);

                messages.save();

                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId].ws;
                    conn.send(JSON.stringify({
                        type: 'NEW_MESSAGE',
                        messages
                    }))
                });
                break;
            default:
                console.log('Unknown message type:', decodedMessage.type);
        }
    });

     ws.on('close',  msg => {
        console.log('client disconnected',  activeConnections[id]);
        delete activeConnections[id];
         const username = Object.keys(activeConnections).map(connId => {
             const connection = activeConnections[connId];
             return connection.user.username
         });

        Object.keys(activeConnections).forEach(connId => {
             const connection = activeConnections[connId].ws;
             connection.send(JSON.stringify({
                 type: 'ACTIVE_USERS',
                 username
             }));
         });


    })
});

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});