import React, {Component, Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch, withRouter} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Container from "./components/UI/Container/Container";
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import Chat from "./containers/Chat/Chat";
import {logout} from "./store/actions";

class App extends Component{


    render() {
      return (
          <Fragment>
              <header>
                  <Toolbar user={this.props.user} logout={this.props.logout}/>
              </header>
              <NotificationContainer/>
              <Container>
                  <Switch>
                      <Route path="/" exact component={Chat} />
                      <Route path="/register" exact component={Register}/>
                      <Route path="/login" exact component={Login}/>
                  </Switch>

              </Container>
          </Fragment>
      )
    }
}


const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logout())
});



export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
