import React from 'react';
import PropTypes from 'prop-types'

import './FormElement.css'

const FormElement = ({propertyName, title, error,  ...props}) => {
    return (
        <div className="form-block">
            <label className="form-label">{title}</label>
            
            <div className="block-input">
                <input
                    className="input"
                    name={propertyName}
                    id={propertyName}
                    {...props}
                />
                {error && (
                    <div style={{color: '#d42f2f', padding: '5px 0'}}>
                        {error}
                    </div>
                )}
            </div>
        </div>
    );
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    error: PropTypes.string,
};

export default FormElement;