import React, {Fragment} from 'react';
import {NavLink} from "react-router-dom";

import './Toolbar.css'

const Toolbar = ({user, logout}) => {
    return (
        <nav className="main-nav">
            <NavLink className="logo" to="/">Chat</NavLink>
            <ul className="nav-ul">

                {user ? (
                    <Fragment>
                          <span className="title-username">
                           Welcome {user.username}
                          </span>
                        <button onClick={logout} className="logout-btn" type="button"><NavLink to="/" className="nav-link">Logout</NavLink></button>
                    </Fragment>

                ):
                    <Fragment>
                        <li className="nav-item"><NavLink className="nav-link" to="/register">Sign up</NavLink></li>
                        <li className="nav-item"><NavLink className="nav-link" to="/login">Login</NavLink></li>
                    </Fragment>
                }

            </ul>
        </nav>
    )
};

export default Toolbar;

