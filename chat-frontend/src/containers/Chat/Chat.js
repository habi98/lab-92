import React, {Component} from 'react';
import {connect} from "react-redux";
import './Chat.css'
class Chat extends Component {
    state = {
        messageText: '',
        onlineUsers: [],
        messages: []
    };
    componentDidMount() {
        let token;
        if (this.props.user && this.props.user.token) {
            token = this.props.user.token
        }

        if (!token) this.props.history.push('/login');
        this.websocket = new WebSocket(`ws://localhost:8000/chat?token=${token}`);

        this.websocket.onmessage = event => {
            const decodedMessage = JSON.parse(event.data);

            if (decodedMessage.type === 'NEW_MESSAGE') {
                this.setState({
                    messages: [...this.state.messages, decodedMessage.messages]
                });
            } else if (decodedMessage.type === 'LATEST_MESSAGES') {
                this.setState({
                    messages: decodedMessage.messages.reverse()

                })

            }

            if (decodedMessage.type === 'ACTIVE_USERS') {
                this.setState({
                    onlineUsers: decodedMessage.username
                });

            }
            if(decodedMessage.type === 'DELETE_USER') {
                console.log(decodedMessage.username, 'delete user')
            }
        };
        this.websocket.onclose = () => {
            setTimeout(() => this.connect, 2000)
        }
    }

    componentWillUnmount() {
        this.websocket.close()
    }

    inputChangeHandler = value => {
        this.setState({
            messageText: value
        })
    };

    sendMessage = () => {
        if(!this.props.user) return null;
        const messages = JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText,
            username: this.props.user.username
        });

        this.websocket.send(messages);
        this.setState({messageText: ''})
    };



    render() {
        return (
            <div style={{padding: '20px 0'}}>
                <div className="block-user">
                    <h3>Online users</h3>

                    <div className="online-user" >
                        {this.state.onlineUsers.length > 0 ? this.state.onlineUsers.map((user, id) => (
                            <p key={id}>{user}</p>
                        )): <span>No Users Online</span>}
                    </div>
                </div>

                <div className="block-chat">
                    <h3>Chat</h3>
                    <div className="chat">

                            {this.state.messages.map((message, id) => (
                                <div key={id} style={{padding: '10px 0'}}>
                                <b>{message.name}: </b>
                                  <span> {message.text}</span>
                               </div>
                            ))}

                    </div>
                    <div>
                        <input value={this.state.messageText}  className="chat-input" type="text" onChange={(e) => this.inputChangeHandler(e.target.value)}/>
                        <button onClick={this.sendMessage}  type="button" disabled={!this.state.messageText} className="chat-send">send</button>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps)(Chat);