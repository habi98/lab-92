import React, {Component, Fragment} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {loginUser} from "../../store/actions";
import {connect} from "react-redux";

const style = {
    background: '#d42f2f',
    color: '#fff',
    borderRadius: '5px',
    padding: '5px 10px',
    marginBottom: '20px'
};


class Login extends Component {
    state = {
      username: '',
      password: ''
    };



    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state});
    };


    render() {
        return (
            <Fragment>
           <h2>Login</h2>
                {this.props.error  && (
                    <div style={style}>
                        {this.props.error.error || this.props.error.global}
                    </div>
                )}
                <form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter username you registered with"
                        autoComplete="current-username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter password"
                        autoComplete="current-password"

                    />

                    <div className="form-button">
                        <button className="btn">
                            Login
                        </button>
                    </div>
                </form>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
   loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);