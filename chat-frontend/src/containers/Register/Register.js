import React, {Component, Fragment} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions";
import {connect} from "react-redux";


const style = {
    background: '#d42f2f',
    color: '#fff',
    borderRadius: '5px',
    padding: '5px 10px',
    marginBottom: '20px'
};

class Register extends Component {
    state = {
        username: '',
        password: ''
    };



    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state});
    };

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };


    render() {
        return (
            <Fragment>
             <h2>Register</h2>
                {this.props.error && this.props.error.global && (
                    <div style={style}>
                        {this.props.error.global}
                    </div>
                )}
                <form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('username')}
                        placeholder="Enter your desired username"
                        autoComplete="new-username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('password')}
                        placeholder="Enter new secure password"
                        autoComplete="new-password"
                    />

                    <div className="form-button">
                        <button className="btn">
                            Register
                        </button>
                    </div>
                </form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser : userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);