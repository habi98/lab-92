import {createBrowserHistory} from "history";
import {applyMiddleware,  compose, createStore, combineReducers} from "redux";
import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";

import userReducer from './userReducer';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router:  connectRouter(history),
    users: userReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore(rootReducer , composeEnhancers(applyMiddleware(thunkMiddleware, routerMiddleware(history))));



export default store